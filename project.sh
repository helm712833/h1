#!/bin/bash
function stop_app() {
    cd tf
    terraform destroy -auto-approve
}
function start_app() {
    cd tf
    terraform init
    terraform validate
    terraform apply -auto-approve
}
function restart_app() {
    cd tf
    terraform destroy -auto-approve
    terraform apply -auto-approve
}
case "$1" in 
    start)   start_app ;;
    stop)    stop_app ;;
    restart) restart_app ;;
    *) echo "usage: $0 start|stop|restart" >&2
       exit 1
       ;;
esac